package pb.wang;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by admin on 2016/3/31.
 */
public class RedisEvaluator {
    public static void main(String[] args) {
        JedisPoolConfig config = new JedisPoolConfig();
        //最大空闲连接数, 应用自己评估，不要超过 Redis每个实例最大的连接数
        config.setMaxIdle(200);
        //最大连接数, 应用自己评估，不要超过Redis每个实例最大的连接数
        config.setMaxTotal(300);
        config.setTestOnBorrow(false);
        config.setTestOnReturn(false);

        String host = "127.0.0.1";
        JedisPool pool = new JedisPool(config, host, 6379, 3000);
        Jedis jedis = null;
        int size = 1000000;
        String longValue = "ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
        String middleValue = "ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
        String sortValue = "1";
        String sortValue2 = "2";
        try {
            jedis = pool.getResource();

            String[] keys = new String[size];
            for (int i =0; i < size; i++ ){
                keys[i] = String.valueOf(i);
            }
            Date start = null;
            Date end = null;
            for (int i =0; i < size; i++ ){
                jedis.del(keys[i]);
            }
            start = new Date();
            for (int i =0; i < size; i++ ){
                jedis.set(keys[i], longValue);
            }
            end = new Date();
            System.out.println("1: " + String.valueOf(end.getTime() - start.getTime()));
            for (int i =0; i < size; i++ ){
                jedis.del(keys[i]);
            }

            start = new Date();
            for (int i =0; i < size; i++ ){
                jedis.lpush(keys[i], longValue);
             //   jedis.lpush(keys[i], sortValue);
             //   jedis.lpush(keys[i], sortValue2);
            }
            end = new Date();
            System.out.println("2: " + String.valueOf(end.getTime() - start.getTime()));
            for (int i =0; i < size; i++ ){
                jedis.del(keys[i]);
            }

            start = new Date();
            for (int i =0; i < size; i++ ){
                jedis.sadd(keys[i], longValue);
             //   jedis.sadd(keys[i], sortValue);
             //   jedis.sadd(keys[i], sortValue2);
            }
            end = new Date();
            System.out.println("3: " + String.valueOf(end.getTime() - start.getTime()));
            for (int i =0; i < size; i++ ){
                jedis.del(keys[i]);
            }

        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        /// ... when closing your application:
        pool.destroy();
    }
}
