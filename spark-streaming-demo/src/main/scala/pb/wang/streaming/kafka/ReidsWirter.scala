package pb.wang.streaming.kafka

import redis.clients.jedis.{Jedis, JedisPool, JedisPoolConfig}

/**
  * Created by admin on 2016/3/31.
  */
@deprecated
object ReidsWirter {
    var jedis: Jedis = null
    def init(): Unit = {
      val config: JedisPoolConfig = new JedisPoolConfig
      //最大空闲连接数, 应用自己评估，不要超过 Redis每个实例最大的连接数
      config.setMaxIdle(200)
      //最大连接数, 应用自己评估，不要超过Redis每个实例最大的连接数
      config.setMaxTotal(300)
      config.setTestOnBorrow(false)
      config.setTestOnReturn(false)

      val host: String = "127.0.0.1"
      val pool: JedisPool = new JedisPool(config, host, 6379, 3000)
      jedis = pool.getResource
    }
  def uninit (): Unit ={
    if (jedis != null) {
      jedis.close
    }
  }
  def incrBy(x:(String, Long)): Unit = {
    jedis.incrBy(x._1, x._2)
  }
  def main(args: Array[String]): Unit = {
    ReidsWirter.init()
    ReidsWirter.incrBy("2", 1)
    ReidsWirter.uninit()
  }
}
