package pb.wang.streaming.kafka

import org.apache.commons.pool2.impl.GenericObjectPoolConfig
import redis.clients.jedis.JedisPool

/**
  * Created by admin on 2016/4/1.
  */

object RedisClient extends Serializable {

  lazy val pool = new JedisPool(new GenericObjectPoolConfig(), redisHost, redisPort, redisTimeout)
  lazy val hook = new Thread {
    override def run = {
      println("Execute hook thread: " + this)
      pool.destroy()
    }
  }
  val redisHost = "127.0.0.1"
  val redisPort = 6379
  val redisTimeout = 30000
  sys.addShutdownHook(hook.run)

  def main(args: Array[String]): Unit = {
    val jedis = RedisClient.pool.getResource
    jedis.incrBy("2", 1)
    RedisClient.pool.returnResourceObject(jedis)

  }
}
