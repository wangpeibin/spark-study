package pb.wang

import java.util.{Date, HashMap}

import org.apache.kafka.clients.producer._

/**
  * Created by admin on 2016/3/31.
  */

object KafkaImpMockProducer {

  object ProducerCallback extends Callback {
    override def onCompletion(metadata: RecordMetadata, exception: Exception): Unit = {
      System.out.println("send to " + metadata.offset());
    }
  }

  def main(args: Array[String]) {
    if (args.length < 2) {
      System.err.println("Usage: KafkaImpMockProducer <metadataBrokerList> <topic> ")
      System.exit(1)
    }

    val Array(brokers, topic) = args

    // Zookeeper connection properties
    val props = new HashMap[String, Object]()
    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers)
    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
      "org.apache.kafka.common.serialization.StringSerializer")
    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
      "org.apache.kafka.common.serialization.StringSerializer")

    val producer = new KafkaProducer[String, String](props)

    // Send some messages
    while (true) {
      val timestamp = new Date().getTime / 1000
      val message = new ProducerRecord[String, String](topic, String.valueOf(timestamp), String.valueOf(timestamp))
      producer.send(message, ProducerCallback)
      Thread.sleep(1000)
    }
  }
}
