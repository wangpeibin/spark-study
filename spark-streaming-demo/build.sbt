import sbt.Keys._

organization := "pb.wang"

name := "spark-streaming"

version := "1.0"

scalaVersion := "2.11.7"

val sparkVersion = "1.6.1"
val kafkaVersion = "0.8.2.2"

//packAutoSettings

libraryDependencies +=  "org.apache.spark" %% "spark-core" % sparkVersion
libraryDependencies +=   "org.apache.spark" %% "spark-bagel" % sparkVersion
libraryDependencies +=   "org.apache.spark" %% "spark-sql" % sparkVersion

libraryDependencies +=   "org.apache.spark" %% "spark-streaming" % sparkVersion
libraryDependencies +=   "org.apache.spark" %% "spark-streaming-kafka" % sparkVersion
libraryDependencies +=   "org.specs" % "specs" % "1.2.5"
libraryDependencies +=   "redis.clients" % "jedis" % "2.7.2"

libraryDependencies += "org.apache.ignite" % "ignite-core" % "1.9.0"