name := "spark-job"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies += "org.apache.spark" %% "spark-core" % "2.1.0"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.1.0"
libraryDependencies += "org.apache.spark" %% "spark-mllib" % "2.1.0"
libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.40"
libraryDependencies += "org.jpmml" % "jpmml-sparkml" % "1.1.6"
libraryDependencies += "org.jpmml" % "pmml-evaluator" % "1.3.5"
libraryDependencies += "org.ansj" % "ansj_seg" % "5.1.1"
libraryDependencies += "org.apache.httpcomponents" % "fluent-hc" % "4.5.3"
libraryDependencies += "com.google.code.gson" % "gson" % "2.8.0"
