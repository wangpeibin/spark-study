package recommendation.book.ml

import java.util.Date

import org.apache.spark.ml.recommendation.ALS
import org.apache.spark.ml.recommendation.ALS.Rating
import org.apache.spark.sql.SparkSession

/**
  * Created by peibin on 2017/4/13.
  */
object ALSDemo {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder()
      .appName("WeiboFeeds")
      .master("local[*]")
      .getOrCreate()

    import spark.implicits._


    val ratings = spark.read.option("header", "true").option("delimiter", ";").csv("""/Users/peibin/workspace/data-mining/书籍评分/BX-Book-Ratings.csv""")
      .map(x => Rating(x.getString(0).toInt, x.getString(1).hashCode, x.getString(2).toFloat))
    val als = new ALS()
      .setRank(5)
      .setMaxIter(10)


    val model = als.fit(ratings)
    model.transform(ratings).show(10)
    // Viewing the result
    // JAXBUtil.marshalPMML(pmml, new StreamResult(System.out));


    val dataFrameToPredict = spark.sparkContext.parallelize(Seq((136139, -2147479511)))
      .toDF("user", "item")
    model.transform(dataFrameToPredict).show()
    println(new Date)
    model.transform(dataFrameToPredict).show()
    println(new Date)
    // val path = "/tmp/als"
    // model.save(path)
    spark.stop()

  }
}
