package spark.code.study.sparksql

import org.apache.spark.sql.SparkSession

/**
  * Created by peibin on 2017/2/3.
  */
object RedditPost {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder()
      .appName("wow")
      .master("local[*]")
      .config("spark.some.config.option", "some-value")
      .getOrCreate()

    // For implicit conversions like converting RDDs to DataFrames
    val wow = spark.read.option("header", "true").csv("file:///share/data/reddit-top-2.5-million-master/data/wow.csv")

    wow.printSchema()
    wow.show(10)
    wow.createOrReplaceTempView("wow")
  }
}
