package spark.code.study

import java.util.Date

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import spark.code.study.sparksql.TaobaoBuyer.Buyer

/**
  * Created by peibin on 2017/6/30.
  */
object PerfectNum {
  val spark = SparkSession
    .builder()
    .appName("wow")
    .master("local[*]")
    .getOrCreate()

  // For implicit conversions like converting RDDs to DataFrames
  import org.apache.spark.sql.functions.input_file_name
  import spark.implicits._

  def isPerfect(num:Int): Boolean  = {
    val start = new Date().getTime
    val result = spark.sparkContext.parallelize(1 to num, 10).filter(num%_ == 0).sum().toInt.equals(num * 2)
    println("cost = " + (new Date().getTime - start))
    result
  }

  def main(args: Array[String]): Unit = {

    List(6, 33550336,33550337).map(x=> println(s"$x is perfect ${isPerfect(x)}"))

  }

}
